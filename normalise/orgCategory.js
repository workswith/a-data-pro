const Feature = require('./Feature')

/**
 * Normalise the organisation types into groups of similar types of
 * company:
 */
const orgCategoryMap = new Feature([
  ['1', 0],
  ['2', 1],
  ['3', 2],
  ['5', 3],
  ['6', 4],
  ['7', 5],
  ['8', 6],
  ['9', 7],
  ['10', 8],
  ['11', 9],
  ['12', 10],
  ['13', 11],
  ['14', 12],
  ['15', 13],
  ['16', 14],
  ['17', 15],
  ['18', 16],
  ['19', 17],
  ['20', 18],
  ['21', 19],
  ['22', 20],
  ['23', 21],
  ['24', 22],
  ['25', 23],
  ['26', 24],
  ['27', 25],
  ['28', 26],
  ['29', 27],
  ['30', 28],
  ['31', 29],
  ['32', 30],
  ['33', 31],
  ['35', 32],
  ['36', 33],
  ['37', 34],
  ['38', 35],
  ['39', 36],
  ['41', 37],
  ['42', 38],
  ['43', 39],
  ['45', 40],
  ['46', 41],
  ['47', 42],
  ['49', 43],
  ['50', 44],
  ['51', 45],
  ['52', 46],
  ['53', 47],
  ['55', 48],
  ['56', 49],
  ['58', 50],
  ['59', 51],
  ['60', 52],
  ['61', 53],
  ['62', 54],
  ['63', 55],
  ['64', 56],
  ['65', 57],
  ['66', 58],
  ['68', 59],
  ['69', 60],
  ['70', 61],
  ['71', 62],
  ['72', 63],
  ['73', 64],
  ['74', 65],
  ['75', 66],
  ['77', 67],
  ['78', 68],
  ['79', 69],
  ['80', 70],
  ['81', 71],
  ['82', 72],
  ['85', 73],
  ['86', 74],
  ['87', 75],
  ['90', 76],
  ['92', 77],
  ['93', 78],
  ['94', 79],
  ['95', 80],
  ['96', 81]
])

module.exports = orgCategoryMap
