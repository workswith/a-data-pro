const Feature = require('./Feature')

const orgAgeMap = new class {
  constructor() {
    this.year = new Date().getFullYear()
  }

  normalise(registeredYear) {
    return this.year - registeredYear
  }
}

module.exports = orgAgeMap
