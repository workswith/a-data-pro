const debug = require('debug')('orgEmployeeMap')
const numeral = require('numeral')

const orgEmployeeMap = new class {
  normalise(input) {
    debug(`input: ${JSON.stringify(input)}`)
    /**
     * First get all the possible values:
     */
    const possibles = [
      input['Number of employees 2017'],
      input['Number of employees 2016'],
      input['Number of employees 2015']
    ]
    debug(`possibles: ${possibles}`)
    /**
     * Now work out the most recent value:
     */
    const recent = possibles.find(element => element !== "")
    /**
     * If there is no value then it means that none of the years had values
     * which is an error:
     */
    if (recent === undefined) {
      debug.extend('errors')(`No values in the record for ${JSON.stringify(input)}`)
      return 0
    }
    if (recent === '0') {
      debug.extend('warning')(`Zero found in the record for ${JSON.stringify(input)}`)
    }
    debug(`recent: ${recent}`)
    return numeral(recent).value()
  }
}

module.exports = orgEmployeeMap
