const DoFn = require('beamish-direct-runner/lib/sdk/harnesses/node-streams/DoFn')

const orgAgeMap = require('./orgAge')
const orgCategoryMap = require('./orgCategory')
const orgEmployeeMap = require('./orgEmployee')
const orgProfitAndLossMap = require('./orgProfitAndLoss')
const orgRurIndMap = require('./orgRurInd')
const orgStatusMap = require('./orgStatus')
const orgTypeMap = require('./orgType')

let row = 1

class NormaliseFn extends DoFn {
  setup() {
    this.header = false
  }

  processElement(c) {
    const input = c.element()

    /**
     * Normalise the age, category, employee counts, status, type and rural indicator:
     */
    const age = orgAgeMap.normalise(input['Date of incorporation'])
    const category = orgCategoryMap.normalise(input['NACE Rev. 2 Code'])
    const employees = orgEmployeeMap.normalise(input)
    const profitAndLoss = orgProfitAndLossMap.normalise(input)
    const rural = orgRurIndMap.normalise(input.City)
    const status = orgStatusMap.normalise(input.Status)
    const type = orgTypeMap.normalise(input['National legal form'])

    if (!this.header) {
      c.output('status,age,category,employees,profitandloss,type,rural\n')
      this.header = true
    }
    c.output(`${status},${age},${category},${employees},${profitAndLoss},${type},${rural}\n`)
  }
}

const featureColumns = input => {
  /**
   * Create one hot vales:
   */
  const age = +input.age
  const category = orgCategoryMap.oneHot(input.category)
  const employees = input.employees
  const profitAndLoss = input.profitandloss
  const rural = orgRurIndMap.oneHot(input.rural)
  const status = input.status
  const type = orgTypeMap.oneHot(input.type)

  return [status, [].concat(age).concat(category).concat(employees).concat(profitAndLoss).concat(rural).concat(type)]
}

class FormatPredictionFn extends DoFn {
  setup() {
    this.row = 1
  }

  processElement(c) {
    const {
      label,
      risk,
      input
    } = c.element()
    let riskLevel
    /**
     * If the actual value is negative then there's already a problem:
     */
    if (label === '1') {
      riskLevel = 'inactive'
    } else {
      /**
       * Otherwise we say that the risk is the same as the probability of the entry
       * being negative:
       */
      if (risk > 0.75) {
        riskLevel = 'high'
      } else if (risk > 0.5) {
        riskLevel = 'medium'
      } else if (risk > 0.25) {
        riskLevel = 'low'
      } else if (risk > 0.05) {
        riskLevel = 'very low'
      } else {
        riskLevel = 'none'
      }
    }

    if (this.row === 1) {
      c.output('[')
    }
    if (this.row > 1) {
      c.output(',\n')
    }
    c.output(`${JSON.stringify({
      name: `Company #${this.row++}`,
      status: label === '0' ? 'Active' : 'Inactive',
      riskLevel,
      risk
    })}`)
  }

  finishBundle(fbc) {
    fbc.output(']')
  }
}

const scale = input => {
  /**
   * Scale the category, status, type and rural indicator:
   */
  const category = orgCategoryMap.scale0To1(input.category)
  const rural = orgRurIndMap.scale0To1(input.rural)
  const status = orgStatusMap.scale0To1(input.status)
  const type = orgTypeMap.scale0To1(input.type)

  return [status, [category, type, rural]]
}

exports.NormaliseFn = NormaliseFn
exports.featureColumns = featureColumns
exports.FormatPredictionFn = FormatPredictionFn
exports.scale = scale
