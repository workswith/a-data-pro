# A Data Pro

Welcome to the `workswith` pipelines for A Data Pro.

The `workswith` pipelines are available as a Docker image. To use them, the easiest approach is to map the input data and the required normalisation scripts into a Docker container as a volume. This is best done with a Docker Compose file.

## Docker Compose File

The structure of the file (in the root directory) is pretty simple, and goes as follows:

```yaml
version: "3.7"

x-default-settings: &default-settings
  image: workswith/pipelines:v0.1.0
  volumes:
    - .:/usr/src/app

x-default-environment-settings: &default-environment-settings
  DEBUG: ${DEBUG}
  INPUT_PATH: ./input
  MODEL_PATH: ./model
  NORMALISE_FN_PATH: /usr/src/app/normalise/index.js
  TASK_ID: companies
  TEMP_PATH: ./tmp

  # Hyper parameters for training and evaluation steps:
  #
  HYPER_EPOCHS: 10
  HYPER_LEARNING_RATE: 0.0001
  HYPER_LOSS: categoricalCrossentropy

services:
  # Analyse the data, to help with normalisation:
  #
  analyse:
    << : *default-settings
    command: /usr/local/lib/node_modules/workswith/pipelines/analyse.js
    environment:
      << : *default-environment-settings

  .
  .
  .
```

The environment variables are set to indicate to the scripts:

* where data should be read from and written to (`INPUT_PATH` and `TEMP_PATH` which are both used in combination with `TASK_ID`);
* where normalisation and scaling functions that are specific to your data can be loaded from (`NORMALISE_FN_PATH`);
* where the trained model can be saved to and read from (`MODEL_PATH`);
* and how to train the model (variables with the prefix `HYPER_`).

To run a task locally, use the Docker Compose `run` command and the name of the task to run. For example:

```shell
docker-compose run --rm analyse
```

Alternatively, install `docker-compose-run`:

```shell
npm i -g docker-compose-run
```

and then from the command-line just use `dcr` followed by the name of the task, like so:

```shell
dcr analyse
```

The commands will use the `TASK_ID` variable to add a prefix to file names, alongside the `INPUT_PATH` and `TEMP_PATH` variables. So with the example above, of `TASK_ID` set to `company` and `INPUT_PATH` set to `./input`, a task like `analyse` would read the file:

```
./input/companies.csv
```

and write its output to:

```
./tmp/companies-analyse.csv
```

After the first task, subsequent tasks will use the output of a previous task as their input, but this will happen automatically.

## Available Commands

### analyse

The `analyse` command will take the input file and return metadata that indicates the number of items in each column, how many are unique, whether any are missing, and so on.

This information can be used to help create the normalisation functions used in the `normalise` step.

The output will be in the `${TEMP_PATH}/${TASK_ID}-analyse.csv` file.

### normalise

The `normalise` command will convert the values in the source data to numbers for use in the training stage. The normalisation functions need to be provided and their location set in the `NORMALISE_FN_PATH` environment variable.

The output will be in the `${TEMP_PATH}/${TASK_ID}-normalise.csv` file.

### balance

The `balance` command is used to boost the number of entries that are members of a particular class, to help with the situation where classes are significantly imbalanced.

The field that contains the class to be checked should be in the `FIELD_TO_BALANCE` environment variable, and the value to check for should be in `VALUE_TO_BALANCE`. The number of additional copies of the row to create should be set in `MULTIPLIER`.

The input will be read from the `${TEMP_PATH}/${TASK_ID}-normalise.csv` file.
The output will be in the `${TEMP_PATH}/${TASK_ID}-balance.csv` file.

### shuffle

The `shuffle` command is used to randomly reorder the data so as to help the ML training step. This is in case the original data is in some kind of order, such as by class.

The input will be read from the `${TEMP_PATH}/${TASK_ID}-normalise.csv` file, or the `${TEMP_PATH}/${TASK_ID}-balance.csv` file.
The output will be in the `${TEMP_PATH}/${TASK_ID}-shuffle.csv` file.

### partition

The `partition` command is used to randomly assign part of the data to a training set and part to an evaluation set. The split is 80/20.

The input will be read from the `${TEMP_PATH}/${TASK_ID}-shuffle.csv` file.
The output will be in two files, the `${TEMP_PATH}/${TASK_ID}-partition-train.csv` and the `${TEMP_PATH}/${TASK_ID}-partition-evaluate.csv` file.

### train

The `train` command trains the model using the training data from the `partition` command.

The number of training epochs is set with `HYPER_EPOCHS`, the learning rate with `HYPER_LEARNING_RATE` and the loss function with `HYPER_LOSS`.

The input will be read from the `${TEMP_PATH}/${TASK_ID}-partition-train.csv` file.
The output will be a model in the `MODEL_PATH` directory.

### evaluate

The `evaluate` command evaluates the model using the evaluation data from the `partition` command.

The input will be read from the `${TEMP_PATH}/${TASK_ID}-partition-evaluate.csv` file.
The model will be loaded from the `MODEL_PATH` directory.
The output will be in the `${TEMP_PATH}/${TASK_ID}-evaluate.csv` file.

### confusion-matrix

The `confusion-matrix` command creates a confusion matrix using the evaluation data from the `partition` command.

The input will be read from the `${TEMP_PATH}/${TASK_ID}-partition-evaluate.csv` file.
The model will be loaded from the `MODEL_PATH` directory.
The output will be in the `${TEMP_PATH}/${TASK_ID}-evaluate.csv` file.
